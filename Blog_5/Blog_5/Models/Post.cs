﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Post
    {
        [Required]
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Title có từ 20 đến 500 ký tự", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Body có tối thiểu 50 ký tự")]
        public String Body { set;get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}