﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Tag
    {
        [Required]
        public int ID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Content có từ 10 đến 100 ký tự", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}