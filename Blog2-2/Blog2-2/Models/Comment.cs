﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    [Table("BinhLuan")]
    public class Comment
    {
        [Range(1, 100)]
        public int ID { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { set; get; }
        [Required]
        public String Author { set; get; }
        [Required]
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}