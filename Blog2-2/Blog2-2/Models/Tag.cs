﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Tag
    {
        [Range(1, 100)]
        public int TagID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Content có từ 10 đến 100 ký tự", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }

    }
}