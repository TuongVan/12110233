﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [Range(1, 100)]
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Title có từ 20 đến 500 ký tự", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Body có tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { set; get; }
        [Required]
        public int AccountID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual Account Accounts { set; get; }
    }
}