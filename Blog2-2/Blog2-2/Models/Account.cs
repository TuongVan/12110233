﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Account
    {
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Firstname có tối đa 100 ký tự")]
        public String FirstName { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Lastname có tối đa 100 ký tự")]
        public String LastName { set; get; }
        [Range(1, 100)]
        public int AccountID { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}