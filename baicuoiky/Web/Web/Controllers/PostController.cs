﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile).Include(p => p.ChuyenMuc);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            Session["Post"] = post;
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ChuyenMucID", "TenCM");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post,int idchuyenmuc)
        {
            if (ModelState.IsValid)
            {
                post.ChuyenMucID = idchuyenmuc;
                post.DayCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;
                db.Posts.Add(post);
                db.SaveChanges();
                return PartialView("viewPost", post);
                //return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ChuyenMucID", "TenCM", post.ChuyenMucID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            post.DateUpdated = DateTime.Now;
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ChuyenMucID", "TenCM", post.ChuyenMucID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details/"+post.ChuyenMucID,"ChuyenMuc");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ChuyenMucID", "TenCM", post.ChuyenMucID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}