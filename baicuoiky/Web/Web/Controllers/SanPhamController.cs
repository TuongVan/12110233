﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class SanPhamController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /SanPham/

        public ActionResult Index()
        {
            var sanphams = db.SanPhams.Include(s => s.DanhMuc);
            return View(sanphams.ToList());
        }

        //
        // GET: /SanPham/Details/5

        public ActionResult Details(int id = 0)
        {
            SanPham sanpham = db.SanPhams.Find(id);
            if (sanpham == null)
            {
                return HttpNotFound();
            }
            return View(sanpham);
        }

        //
        // GET: /SanPham/Create

        public ActionResult Create()
        {
            ViewBag.DanhMucID = new SelectList(db.DanhMucs, "ID", "TenDM");
            return View();
        }

        //
        // POST: /SanPham/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SanPham sanpham,int iddanhmuc)
        {
            if (ModelState.IsValid)
            {
                sanpham.DanhMucID = iddanhmuc;
                db.SanPhams.Add(sanpham);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DanhMucID = new SelectList(db.DanhMucs, "ID", "TenDM", sanpham.DanhMucID);
            return View(sanpham);
        }

        //
        // GET: /SanPham/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SanPham sanpham = db.SanPhams.Find(id);
            if (sanpham == null)
            {
                return HttpNotFound();
            }
            ViewBag.DanhMucID = new SelectList(db.DanhMucs, "ID", "TenDM", sanpham.DanhMucID);
            return View(sanpham);
        }

        //
        // POST: /SanPham/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SanPham sanpham)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sanpham).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DanhMucID = new SelectList(db.DanhMucs, "ID", "TenDM", sanpham.DanhMucID);
            return View(sanpham);
        }

        //
        // GET: /SanPham/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SanPham sanpham = db.SanPhams.Find(id);
            if (sanpham == null)
            {
                return HttpNotFound();
            }
            return View(sanpham);
        }

        //
        // POST: /SanPham/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SanPham sanpham = db.SanPhams.Find(id);
            db.SanPhams.Remove(sanpham);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}