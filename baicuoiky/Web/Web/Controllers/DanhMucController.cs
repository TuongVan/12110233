﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class DanhMucController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /DanhMuc/

        public ActionResult Index()
        {
            return View(db.DanhMucs.ToList());
        }

        //
        // GET: /DanhMuc/Details/5

        public ActionResult Details(int id = 0)
        {
            DanhMuc danhmuc = db.DanhMucs.Find(id);
            Session["DanhMuc"] = danhmuc;
            ViewData["iddanhmuc"] = id;
            if (danhmuc == null)
            {
                return HttpNotFound();
            }
            return View(danhmuc);
        }

        //
        // GET: /DanhMuc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DanhMuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhMuc danhmuc)
        {
            if (ModelState.IsValid)
            {
                db.DanhMucs.Add(danhmuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(danhmuc);
        }

        //
        // GET: /DanhMuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DanhMuc danhmuc = db.DanhMucs.Find(id);
            if (danhmuc == null)
            {
                return HttpNotFound();
            }
            return View(danhmuc);
        }

        //
        // POST: /DanhMuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhMuc danhmuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danhmuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(danhmuc);
        }

        //
        // GET: /DanhMuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DanhMuc danhmuc = db.DanhMucs.Find(id);
            if (danhmuc == null)
            {
                return HttpNotFound();
            }
            return View(danhmuc);
        }

        //
        // POST: /DanhMuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DanhMuc danhmuc = db.DanhMucs.Find(id);
            db.DanhMucs.Remove(danhmuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}