﻿$(function () {
    $("#myform").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (data) {
                $("#resuft").prepend(data)
            }
        });
        $("#myform").trigger("reset");
    });
});