namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SanPhams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenSP = c.String(nullable: false),
                        Gia = c.Single(nullable: false),
                        ThongTin = c.String(),
                        DanhMucID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DanhMucs", t => t.DanhMucID, cascadeDelete: true)
                .Index(t => t.DanhMucID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SanPhams", new[] { "DanhMucID" });
            DropForeignKey("dbo.SanPhams", "DanhMucID", "dbo.DanhMucs");
            DropTable("dbo.SanPhams");
        }
    }
}
