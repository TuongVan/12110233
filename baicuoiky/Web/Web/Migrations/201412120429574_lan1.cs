namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DanhMucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenDM = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.SanPhams");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SanPhams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenSP = c.String(nullable: false),
                        Gia = c.Single(nullable: false),
                        ThongTin = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.DanhMucs");
        }
    }
}
