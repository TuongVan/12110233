﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class DanhMuc
    {
        [Key]
        [Required]
        public int ID { set; get; }
        [Required]
        public string TenDM { set; get; }
        public virtual ICollection<SanPham> SanPhams { set; get; }
    }
}