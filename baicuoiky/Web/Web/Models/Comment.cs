﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }
        [Required]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Body có tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}