﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ChuyenMuc
    {
        public int ChuyenMucID { set; get; }
        public String TenCM { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}