﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SanPham
    {
        [Key]
        [Required]
        public int ID { set; get; }
        [Required]
        public string TenSP { set; get; }
        [Required]
        public float Gia { set; get; }
        public string ThongTin { set; get; }
        public virtual DanhMuc DanhMuc { set; get; }
        public int DanhMucID { set; get; }
    }
}